//
//  ViewController.swift
//  MyPlaces
//
//  Created by Rumeysa Bulut on 6.01.2020.
//  Copyright © 2020 Rumeysa Bulut. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation // to get user location
import CoreData

class ViewController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate {

    @IBOutlet weak var mapView: MKMapView!
    var locationManager = CLLocationManager()
    
    @IBOutlet weak var nameText: UITextField!
    @IBOutlet weak var commentText: UITextField!
    var chosenLatitude = Double()
    var chosenLongitude = Double()
    
    var selectedTitle = ""
    var selectedID : UUID?
    
    var annotationTitle = ""
    var annotationComment = ""
    var annotationLat = Double()
    var annotationLon = Double()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mapView.delegate = self
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest // consumes battery too much so you may choose another accuracy metric such as kCLLocationAccuracyKilometer. The suitable one to the condition should be chosen
        locationManager.requestWhenInUseAuthorization() // while the app is being used
        locationManager.startUpdatingLocation()
        
        let gestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(chooseLocation(gestureRecognizer:)))
        gestureRecognizer.minimumPressDuration = 3 // after 3 seconds, it will recognize the tap
        mapView.addGestureRecognizer(gestureRecognizer)
        
        if selectedTitle != "" {
            // get related data from CoreData
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let context = appDelegate.persistentContainer.viewContext
            
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Places")
            let idStr = selectedID!.uuidString
            fetchRequest.predicate = NSPredicate(format: "id = %@", idStr) // fetches only wanted ids
            fetchRequest.returnsObjectsAsFaults = false
            
            do {
                let results = try context.fetch(fetchRequest)
                if results.count > 0 {
                    for result in results as! [NSManagedObject] {
                        
                        if let title = result.value(forKey: "title") as? String {
                            annotationTitle = title
                        
                            if let subtitle = result.value(forKey: "subtitle") as? String {
                                annotationComment = subtitle
                                
                                if let latitude = result.value(forKey: "latitude") as? Double {
                                    annotationLat = latitude
                                    
                                    
                                    if let longitude = result.value(forKey: "longitude") as? Double {
                                        annotationLon = longitude
                                        
                                        let annotation = MKPointAnnotation()
                                        annotation.title = annotationTitle
                                        annotation.subtitle = annotationComment
                                        let coord = CLLocationCoordinate2D(latitude: annotationLat, longitude: annotationLon)
                                        annotation.coordinate = coord
                                        
                                        mapView.addAnnotation(annotation)
                                        nameText.text = annotationTitle
                                        commentText.text = annotationComment
                                        
                                        locationManager.stopUpdatingLocation() // We don't want to update the loation when user moves. We want to show what user saved.
                                        // Map should be open over the selected place. When simulator's location is set to another coordinate, wherever you select, it opens simulator's location.
                                        
                                        let span = MKCoordinateSpan(latitudeDelta: 0.05, longitudeDelta: 0.05)
                                        let region = MKCoordinateRegion(center: coord, span: span)
                                        mapView.setRegion(region, animated: true)
                                    }
                                }
                            }
                        }
                    }
                }
            } catch {
                
            }
        } else {
            // add new data
        }
    }
    
    @objc func chooseLocation(gestureRecognizer: UILongPressGestureRecognizer) {
        if gestureRecognizer.state == .began {  // if long press has started
            let touchedPoint = gestureRecognizer.location(in: self.mapView)
            let touchedCoordinates = self.mapView.convert(touchedPoint, toCoordinateFrom: self.mapView)  // to convert touched points to coordinates
            
            chosenLatitude = touchedCoordinates.latitude  // to save in db
            chosenLongitude = touchedCoordinates.longitude // to save in db
            
            let annotation = MKPointAnnotation()
            annotation.coordinate = touchedCoordinates
            annotation.title = nameText.text
            annotation.subtitle = commentText.text
            self.mapView.addAnnotation(annotation)  // adds our pinned place to mapView
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        // to make sure thay locationmanager doesn't set the map to the default coordinates.
        if selectedTitle != "" {
            let location = CLLocationCoordinate2D(latitude: locations[0].coordinate.latitude, longitude: locations[0].coordinate.longitude)
            let span = MKCoordinateSpan(latitudeDelta: 0.05, longitudeDelta: 0.05) // delta determines zoom level. The smaller delta, the closer map
            let region = MKCoordinateRegion(center: location, span: span)
            mapView.setRegion(region, animated: true)
        }
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        // to not to show user's current location with pin
        if annotation is MKUserLocation {
            return nil
        }
        
        let reuseId = "myAnnotation" // doesn't matter how you name it
        var pinView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseId) as? MKPinAnnotationView
        
        if pinView == nil {
            pinView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
            pinView?.canShowCallout = true // can show extra information adding to bubbles
            pinView?.tintColor = UIColor.blue // changes the pinView color from red to any color
            
            let button = UIButton(type: UIButton.ButtonType.detailDisclosure)
            pinView?.rightCalloutAccessoryView = button // adds a button to canShowCallout
        } else {
            pinView?.annotation = annotation
        }
        
        return pinView
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        if selectedTitle != "" {
            let requestlLocation = CLLocation(latitude: annotationLat, longitude: annotationLon)
            
            CLGeocoder().reverseGeocodeLocation(requestlLocation) { (placemarks, error) in
                if let placemark = placemarks { // placemarks optional so check if it isn't empty
                    if placemark.count > 0 {
                        let newPlacemark = MKPlacemark(placemark: placemark[0])
                        let item = MKMapItem(placemark: newPlacemark) // map item will open the map
                        item.name = self.annotationTitle
                        let launchOptions = [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeWalking] // declares mode of map in first opening
                        item.openInMaps(launchOptions: launchOptions) // opens the map
                    }
                }
            }
        }
    }
    
    @IBAction func saveButtonClicked(_ sender: Any) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        let newPlace = NSEntityDescription.insertNewObject(forEntityName: "Places", into: context)
        newPlace.setValue(nameText.text, forKey: "title")
        newPlace.setValue(commentText.text, forKey: "subtitle")
        newPlace.setValue(chosenLatitude, forKey: "latitude")
        newPlace.setValue(chosenLongitude, forKey: "longitude")
        newPlace.setValue(UUID(), forKey: "id")
        
        do {
            try context.save()
            print("place saved")
        } catch {
            print("Error while saving")
        }
        
        // To see newly saved placed on the list without killing the app
        NotificationCenter.default.post(name: NSNotification.Name("newPlace"), object: nil)
        navigationController?.popViewController(animated: true)
    }
    

}

